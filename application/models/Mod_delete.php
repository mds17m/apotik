<?php

class Mod_delete extends CI_Model {

	var $table = 'supplier'; //nama tabel dari database
	var $column_order = array('kode_supplier', 'nama_supplier','alamat','telpon'); //field yang ada di table user
	var $column_search = array('kode_supplier', 'nama_supplier','alamat','telpon'); //field yang diizin untuk pencarian 
	var $order = array('kode_supplier' => 'asc'); // default order 

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function delSupp($id)
	{
		    $this->db->where('kode_supplier', $id);
            $this->db->delete('supplier');	
            return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	public function delUser($id)
	{
		    $this->db->where('id_user', $id);
            $this->db->delete('sys_users');	
            return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	public function delObat($id)
	{
		    $this->db->where('kode_obat', $id);
            $this->db->delete('obat');	
            return ($this->db->affected_rows() > 0) ? TRUE : FALSE;
	}

	public function delDb($id){
		$this->db->where('id', $id);
		$this->db->delete('sys_database');	
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;	
	}

	public function delPer($id){
		$this->db->where('id_akses', $id);
		$this->db->delete('hak_akses');	
		return ($this->db->affected_rows() > 0) ? TRUE : FALSE;	
	}
	

    

}
