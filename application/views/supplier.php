<?php
	$this->load->view('header');
?>
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<?php $this->load->view('nav')?>
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<?php $this->load->view('sidebar')?>
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<div class="m-content">
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
										</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									<ul class="m-portlet__nav">
										<li class="m-portlet__nav-item">
											<a href="#" data-toggle="modal" id="tambah" data-target="#m_modal_1" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
												<span>
													<i class="la la-plus"></i>
													<span>Tambah Data</span>
												</span>
											</a>
										</li>
										<li class="m-portlet__nav-item">
											<a href="<?=base_url('exportSupplier')?>" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
												<span>
													<i class="fa fa-file-export"></i>
													<span>Export</span>
												</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="m-portlet__body">
								<table id="supplier" class="table table-striped- table-bordered table-hover table-checkable">
									<thead>
										<tr>
											<th>Kode Supplier</th>
											<th>Nama Supplier</th>
											<th>Alamat</th>
											<th>Telepon</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php
	$this->load->view('modal');
	$this->load->view('footer');
?>


<script type="text/javascript">
    var table;
    $(document).ready(function() {

        table = $('#supplier').DataTable({ 

            "processing": true, 
            "serverSide": true, 
            "order": [], 
            
            "ajax": {
                "url": 'http://localhost/apotek/getSupplier',
                "type": "POST"
            },

            
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],

		});
		

		$("#tambah").click(function(){
			$("#kdSup").val("")
			$("#nama").val("")
			$("#telepon").val("")
			$("#alamat").val("")
    		$.ajax({
				url : 'http://localhost/apotek/kodeSupp',
				success: function(res){
					var dataKode;
					var newKode = res.toString();
					console.log(newKode.length)
					if(newKode.length == 1){
						dataKode = 'SP00'+newKode;
					}else if(newKode.length == 2){
						dataKode = 'SP0'+newKode;
					}else{
						dataKode = 'SP'+newKode;
					}
					$('#kdSup').val(dataKode)
					$("#jenis").val("tambah")
					$("#title").html("Tambah Data Supplier")
				}
			})
  		});

    });


	function updateSupp(){
		$.ajax({
			type : "POST",
			url : '<?=base_url('saveSupplier')?>',
			data: $("#editSuppForm").serialize(),
			success: function(report){
				$('#m_modal_1').modal('toggle')
				swal({
					type: 'success',
					title: 'Data berhasil di simpan',
					showConfirmButton: false,
					timer: 1500
				});
				refreshDataTable(report)
			}
		})
	}

	function refreshDataTable(res) {
		var obj = JSON.parse(res);
		var dataRes = [];
			for (let index = 0; index < obj.length; index++) {
				var kode = obj[index].kode_supplier;
				var kd_sup = parseInt(kode.replace('SP',''))
				var arraynew = [obj[index].kode_supplier,obj[index].nama_supplier,obj[index].alamat,obj[index].telepon,'<center><button data-toggle="modal" onclick="getKodeEdit('+kd_sup+')" data-target="#m_modal_1" kode="'+obj[index].kode_supplier+'" id="btnGetSupp'+obj[index].kode_supplier+'" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"> <i class="flaticon-edit"></i> </button> <button kode="'+obj[index].kode_supplier+'" onclick="getKodeDelete('+kd_sup+')" id="btnDelete'+obj[index].kode_supplier+'" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"> <i class="flaticon-delete-1"></i> </button> </center>'];
				dataRes.push(arraynew);   
			}
		$('#supplier').DataTable( {
			destroy: true,
			data: dataRes,
			columns: [
				{ title: "Kode Supplier" },
				{ title: "Nama Supplier" },
				{ title: "Alamat" },
				{ title: "Telepon" },
				{ title: "Aksi" }
			]
		} );
	}

	function getSupplierDetail(kd){
		$.ajax({
			type : "POST",
			url : 'http://localhost/apotek/getSupplierEdit',
			data : {dataId : kd},
			success: function(res){
				console.log(res)
				var obj = JSON.parse(res);
				console.log(obj)
				$("#kdSup").val(obj.kode)
				$("#nama").val(obj.nama)
				$("#telepon").val(obj.telp)
				$("#alamat").val(obj.alamat)
				$("#jenis").val("update")
				$("#title").html("Edit Data Supplier")
			}
		})
	}

	function getSupplierDelete(kd){
		swal({
            title: 'Delete data?',
            text: "Data akan di hapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus',
            cancelButtonText: 'tidak',
            reverseButtons: true
        }).then(function(result){
			if (result.value) {
				$.ajax({
					type : "POST",
					url : 'http://localhost/apotek/delSupplier',
					data : {dataId : kd},
					success: function(res){
						swal({
							type: 'success',
							title: 'Data berhasil di hapus',
							showConfirmButton: false,
							timer: 1500
						});	
						refreshDataTable(res)
					}
				})
			}else if (result.dismiss === 'cancel') {
                swal({
                    type: 'info',
                    title: 'Data tidak dihapus',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
		});
	}

	function getKodeEdit(kode){
		console.log("akses get kode edit")
		var dataKode;
		var newKode = kode.toString();
		if(newKode.length == 1){
			dataKode = 'SP00'+newKode;
		}else if(newKode.length == 2){
			dataKode = 'SP0'+newKode;
		}else{
			dataKode = 'SP'+newKode;
		}
		getSupplierDetail(dataKode)
	}

	function getKodeDelete(kode){
		console.log("akses get kode delete")
		var dataKode;
		var newKode = kode.toString();
		if(newKode.length == 1){
			dataKode = 'SP00'+newKode;
		}else if(newKode.length == 2){
			dataKode = 'SP0'+newKode;
		}else{
			dataKode = 'SP'+newKode;
		}
		getSupplierDelete(dataKode)
	}


</script>
