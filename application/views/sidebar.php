<?php
    $menuMaster = "";
    if(isset($master)){
        $menuMaster = $master;
    }

?>

<!-- BEGIN: Left Aside -->
<button class="m-aside-left-close " id="m_aside_left_close_btn"><i class="la la-close"></i></button>
	<div id="m_aside_left" class="m-grid__item	m-aside-left " style="background:rgb(56, 72, 86)!important">
        <!-- BEGIN: Aside Menu -->
        <div id="m_ver_menu" class="m-aside-menu " style="background:rgb(56, 72, 86)!important" m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
            <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
            <?php
                if ($this->session->userdata('MenuDashboard') == 'Dashboard') {
            ?>
                <li class="m-menu__item  m-menu__item--active" aria-haspopup="true">
                    <a href="<?=base_url('home')?>" class="m-menu__link ">
                        <i class="m-menu__link-icon flaticon-line-graph text-white"></i>
                        <span class="m-menu__link-title"> 
                            <span class="m-menu__link-wrap"> 
                                <span class="m-menu__link-text text-white">Dashboard</span>
                            </span>
                        </span>
                    </a>
                </li>
            <?php
                }
            ?>

            <?php
                if ($this->session->userdata('MenuMaster') == 'Master') {
            ?>
                <li class="m-menu__item  m-menu__item--submenu <?=$menuMaster?>" aria-haspopup="true" m-menu-submenu-toggle="hover">
                    <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-layers text-white"></i>
                        <span class="m-menu__link-text text-white">Master</span>
                        <i class="m-menu__ver-arrow la la-angle-right text-white"></i>
                    </a>
                    <div class="m-menu__submenu myActive"><span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                        <?php
                            if ($this->session->userdata('Supplier') == 'Supplier') {
                        ?>
                            <li class="m-menu__item mySubMenu" aria-haspopup="true"><a href="<?=base_url('supplier')?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot la la-users text-white"><span></span></i><span class="m-menu__link-text text-white">Supplier</span></a></li>
                        <?php
                            }
                        ?>
                        <?php
                            if ($this->session->userdata('Obat') == 'Obat') {
                        ?>
                            <li class="m-menu__item mySubMenu" aria-haspopup="true"><a href="<?=base_url('obat')?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot la la-medkit text-white"><span></span></i><span class="m-menu__link-text text-white">Obat</span></a></li>
                        <?php
                            }
                        ?>    
                        <?php
                            if ($this->session->userdata('Pengguna') == 'Pengguna') {
                        ?>
                            <li class="m-menu__item mySubMenu" aria-haspopup="true"><a href="<?=base_url('pengguna')?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot la la-user text-white"><span></span></i><span class="m-menu__link-text text-white">Pengguna</span></a></li>
                        <?php
                            }
                        ?> 
                        <?php
                            if ($this->session->userdata('Role') == 'Role') {
                        ?>
                            <li class="m-menu__item mySubMenu" aria-haspopup="true"><a href="<?=base_url('role')?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot flaticon-clipboard text-white"><span></span></i><span class="m-menu__link-text text-white">Roles</span></a></li>
                        <?php
                            }
                        ?> 
                        </ul>
                    </div>
                </li>
            <?php
                }
            ?>
            <?php
                if ($this->session->userdata('MenuTransaksi') == 'Transaksi') {
            ?>
                <li class="m-menu__item  m-menu__item--submenu " aria-haspopup="true" m-menu-submenu-toggle="hover">
                    <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-graphic-2 text-white"></i>
                        <span class="m-menu__link-text text-white">Transaksi</span>
                        <i class="m-menu__ver-arrow la la-angle-right text-white"></i>
                    </a>
                    <div class="m-menu__submenu myActive"><span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <?php
                                if ($this->session->userdata('Pembelian') == 'Pembelian') {
                            ?>
                                <li class="m-menu__item mySubMenu" aria-haspopup="true"><a href="<?=base_url('supplier')?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot flaticon-cart text-white"><span></span></i><span class="m-menu__link-text text-white">Pembelian</span></a></li>
                            <?php
                                }
                            ?>
                            <?php
                                if ($this->session->userdata('Penjualan') == 'Penjualan') {
                            ?>
                                <li class="m-menu__item mySubMenu" aria-haspopup="true"><a href="components/base/tabs/line.html" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot la la-dollar text-white"><span></span></i><span class="m-menu__link-text text-white">Penjualan</span></a></li>
                            <?php
                                }
                            ?>
                        </ul>
                    </div>
                </li>
            <?php
            }
            ?>   
             <?php
                if ($this->session->userdata('MenuLaporan') == 'Laporan') {
            ?>   
                <li class="m-menu__item  m-menu__item--submenu " aria-haspopup="true" m-menu-submenu-toggle="hover">
                    <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-graphic-2 text-white"></i>
                        <span class="m-menu__link-text text-white">Laporan</span>
                        <i class="m-menu__ver-arrow la la-angle-right text-white"></i>
                    </a>
                    <div class="m-menu__submenu myActive"><span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                        <?php
                            if ($this->session->userdata('LapPembelian') == 'LapPembelian') {
                        ?>
                            <li class="m-menu__item mySubMenu" aria-haspopup="true"><a href="<?=base_url('lap-pembelian')?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot flaticon-cart text-white"><span></span></i><span class="m-menu__link-text text-white">Pembelian</span></a></li>
                        <?php
                            }
                        ?> 

                        <?php
                            if ($this->session->userdata('LapPenjualan') == 'LapPenjualan') {
                        ?>
                            <li class="m-menu__item mySubMenu" aria-haspopup="true"><a href="components/base/tabs/line.html" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot la la-dollar text-white"><span></span></i><span class="m-menu__link-text text-white">Penjualan</span></a></li>

                        <?php
                            }
                        ?>                        
                            <li class="m-menu__item mySubMenu" aria-haspopup="true"><a href="<?=base_url('lap-stok')?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot flaticon-cart text-white"><span></span></i><span class="m-menu__link-text text-white">Stok</span></a></li>
                        </ul>
                    </div>
                </li>
            <?php
                }
            ?>  
                 <li class="m-menu__item  m-menu__item--submenu " aria-haspopup="true" m-menu-submenu-toggle="hover">
                    <a href="javascript:;" class="m-menu__link m-menu__toggle">
                        <i class="m-menu__link-icon flaticon-graphic-2 text-white"></i>
                        <span class="m-menu__link-text text-white">Utility</span>
                        <i class="m-menu__ver-arrow la la-angle-right text-white"></i>
                    </a>
                    <div class="m-menu__submenu myActive"><span class="m-menu__arrow"></span>
                        <ul class="m-menu__subnav">
                            <li class="m-menu__item mySubMenu" aria-haspopup="true"><a href="<?=base_url('database')?>" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot flaticon-cart text-white"><span></span></i><span class="m-menu__link-text text-white">Backup Database</span></a></li>
                            <li class="m-menu__item mySubMenu" aria-haspopup="true"><a href="components/base/tabs/line.html" class="m-menu__link "><i class="m-menu__link-bullet m-menu__link-bullet--dot la la-dollar text-white"><span></span></i><span class="m-menu__link-text text-white">Konfigurasi App</span></a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <!-- END: Aside Menu -->
    </div>