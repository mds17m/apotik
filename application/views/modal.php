<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="title">Tambah Data Obat</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?=base_url('saveSupplier')?>" id="editSuppForm" method="post">
					<div class="row">
						<div class="col-md-3">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--pill">
									<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1">kode</span></div>
									<input type="text" name="kode" class="form-control m-input" value="" id="kdSup" aria-describedby="basic-addon1" readonly>
									<input type="hidden" name="jenis" id="jenis">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--square">
									<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-phone"></i></span></div>
									<input type="text" name="telepon" class="form-control m-input" id="telepon" placeholder="Telepon" aria-describedby="basic-addon1" required>
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--square">
									<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-user"></i></span></div>
									<input type="text" name="nama" class="form-control m-input" id="nama" placeholder="Nama Supplier" aria-describedby="basic-addon1" required>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--square">
									<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-map-marker"></i></span></div>
									<input type="text" name="alamat" class="form-control m-input" id="alamat" placeholder="Alamat" aria-describedby="basic-addon1" required>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" onclick="updateSupp()" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modal_obat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="titleObat">Tambah Data Obat</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?=base_url('saveObat')?>" id="editObatForm" method="post">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--pill">
									<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1">kode</span></div>
									<input type="text" name="kodeObat" class="form-control m-input" value="" id="kdObat" aria-describedby="basic-addon1" readonly>
									<input type="hidden" name="jenisObat" id="jenisObat">
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--square">
									<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-map-marker"></i></span></div>
									<input type="text" name="namaObat" class="form-control m-input" id="namaObat" placeholder="Nama Obat" aria-describedby="basic-addon1" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--square">
									<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-map-marker"></i></span></div>
									<input type="text" name="minstok" class="form-control m-input" id="minstok" placeholder="Minimal Stok" aria-describedby="basic-addon1" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--square">
									<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-phone"></i></span></div>
									<input type="text" name="hargaBeli" class="form-control m-input" id="hargaBeli" placeholder="Harga Beli" aria-describedby="basic-addon1" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--square">
									<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-user"></i></span></div>
									<input type="text" name="hargaJual" class="form-control m-input" id="hargaJual" placeholder="Harga Jual" aria-describedby="basic-addon1" required>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--square">
									<!-- <div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-user"></i></span></div> -->
									<select class="form-control m-input" name="satuan" id="satuan">
										<option value="1">Tube</option>
										<option value="2">Strip</option>
										<option value="3">Kotak</option>
										<option value="4">Box</option>
										<option value="5">Botol</option>
									</select>
									<!-- <input type="text" name="satuan" class="form-control m-input" id="satuan" placeholder="Satuan" aria-describedby="basic-addon1" required> -->
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" onclick="updateObat()" class="btn btn-primary" data-dismiss="modal">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalUser" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="titleUser">Tambah Data Pengguna</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?=base_url('saveUser')?>" id="editUserForm" method="post">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--square">
									<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-map-marker"></i></span></div>
									<input type="hidden" name="jenisUser" id="jenisUser">
									<input type="hidden" name="userid" id="userid">
									<input type="text" name="username" class="form-control m-input" id="username" placeholder="Username" aria-describedby="basic-addon1" required>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--square">
									<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-map-marker"></i></span></div>
									<input type="password" name="password" class="form-control m-input" id="password" placeholder="Password" aria-describedby="basic-addon1" required>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--square">
									<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-phone"></i></span></div>
									<input type="text" name="namaUser" class="form-control m-input" id="namaUser" placeholder="Nama User" aria-describedby="basic-addon1" required>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--square">
									<!-- <div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-user"></i></span></div> -->
									<select class="form-control m-input" name="hak_akses" id="hak_akses">
										<?php
											if(isset($hakAkses)){
												foreach ($hakAkses as $akses) {?>

													<option value="<?=$akses->id_akses?>"><?=$akses->nama_akses?></option>

												<?php 
												}
											}
										?>
									</select>
									<!-- <input type="text" name="satuan" class="form-control m-input" id="satuan" placeholder="Satuan" aria-describedby="basic-addon1" required> -->
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">Status</label>
								<div class="col-3">
									<span class="m-switch m-switch--outline m-switch--info">
										<label>
											<input type="checkbox" id="status" name="status">
											<span></span>
										</label>
									</span>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div id="lableWrongUser">
								
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" onclick="updateUser()" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalRole" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="titleUser">Data Role ( Hak Akses ) </h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form action="<?=base_url('saveUser')?>" id="" method="post">
					<input type="hidden" id="jenisPermission" >
					<input type="hidden" id="idRole" >
					<div class="row">
						<div class="col-md-12">
							<div class="form-group m-form__group">
								<div class="input-group m-input-group m-input-group--square">
									<div class="input-group-prepend"><span class="input-group-text" id="basic-addon1"><i class="la la-map-marker"></i></span></div>
									<input type="text" name="namaRole" class="form-control m-input" id="namaRole" placeholder="Hak Akses" aria-describedby="basic-addon1" required>
								</div>
							</div>
						</div>
						<div class="col-md-12">
							<div class="row">
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Dashboard</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="ParentDashboard" onclick="cekVal('ParentDashboard')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Master</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="ParentMaster" onclick="cekVal('ParentMaster')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Transaksi</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="ParentTransaksi" onclick="cekVal('ParentTransaksi')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Laporan</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="ParentLaporan" onclick="cekVal('ParentLaporan')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<hr>
						<div class="col-md-12" id="menuParentMaster">
							<label class="col-3 col-form-label">Sub Menu Master</label>
							<hr>
							<div class="row">
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Supplier</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="SubSupplier" onclick="cekVal('SubSupplier')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Obat</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="SubObat" onclick="cekVal('SubObat')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Pengguna</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="SubPengguna" onclick="cekVal('SubPengguna')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Role</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="SubRole" onclick="cekVal('SubRole')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12" id="menuParentTransaksi">
							<label class="col-3 col-form-label">Sub Menu Transaksi</label>
							<hr>
							<div class="row">
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Pembelian</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="SubPembelian" onclick="cekVal('SubPembelian')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Penjualan</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="SubPenjualan" onclick="cekVal('SubPenjualan')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12" id="menuParentLaporan">
							<label class="col-3 col-form-label">Sub Menu Laporan</label>
							<hr>
							<div class="row">
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Laporan Pembelian</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="LapPembelian" onclick="cekVal('LapPembelian')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Lap Penjualan</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="LapPenjualan" onclick="cekVal('LapPenjualan')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Laporan Stok</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="LapStok" onclick="cekVal('LapStok')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
								<div class="col-md-3">
									<div class="m-form__group form-group row">
										<label class="col-3 col-form-label">Laporan</label>
										<div class="col-12">
											<span class="m-switch m-switch--outline m-switch--info">
												<label>
													<input type="checkbox" id="Laporan" onclick="cekVal('Laporan')" data-val="0" name="status">
													<span></span>
												</label>
											</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" onclick="setPermission()" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
