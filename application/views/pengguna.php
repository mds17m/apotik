<?php
	$this->load->view('header');
?>
	<body class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">
		<div class="m-grid m-grid--hor m-grid--root m-page">
			<?php $this->load->view('nav')?>
			<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">
				<?php $this->load->view('sidebar')?>
				<div class="m-grid__item m-grid__item--fluid m-wrapper">
					<div class="m-content">
						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<h3 class="m-portlet__head-text">
										</h3>
									</div>
								</div>
								<div class="m-portlet__head-tools">
									<ul class="m-portlet__nav">
										<li class="m-portlet__nav-item">
											<a href="#" data-toggle="modal" id="tambah" data-target="#modalUser" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
												<span>
													<i class="la la-plus"></i>
													<span>Tambah Data</span>
												</span>
											</a>
										</li>
										<li class="m-portlet__nav-item">
											<a href="<?=base_url('exportUser')?>" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--icon m-btn--air">
												<span>
													<i class="fa fa-file-export"></i>
													<span>Export</span>
												</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
							<div class="m-portlet__body">
								<table id="User" class="table table-striped- table-bordered table-hover table-checkable">
									<thead>
										<tr>
											<th>Nama User</th>
											<th>Username</th>
											<th>Hak Akses</th>
											<th>Status</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
<?php
	$this->load->view('modal');
	$this->load->view('footer');
?>


<script type="text/javascript">
    var table;
    $(document).ready(function() {

        //datatables
        table = $('#User').DataTable({ 

            "processing": true, 
            "serverSide": true, 
            "order": [], 
            
            "ajax": {
                "url": '<?=base_url('getUser')?>',
                "type": "POST"
            },

            
            "columnDefs": [
            { 
                "targets": [ 0 ], 
                "orderable": false, 
            },
            ],

		});
		

		$("#tambah").click(function(){
			$("#userid").val("")
			$("#username").val("")
			$("#namaUser").val("")
			$("#password").val("")
			$("#password").attr("placeholder", "Password")
			$("#status").removeAttr("checked", "checked")
    		$.ajax({
				url : '<?=base_url('kodeSupp')?>',
				success: function(res){
					$("#jenisUser").val("tambah")
					$("#titleUser").html("Tambah Data Pengguna")
				}
			})
  		});

    });

	function updateUser(){
        var jenis = $("#jenisUser").val()
        var username = $("#username").val()
        var password = $("#password").val()
        var nama = $("#namaUser").val()
        var akses = $("#hak_akses").val()

        if (username == "") {
            $("#lableWrongUser").append('<span class="text-danger"> -Username tidak boleh kosong </span><br>')
        }
        if (jenis == "tambah") {
            if (password == "") {
                $("#lableWrongUser").append('<span class="text-danger"> -Password tidak boleh kosong </span><br>')
            }
        }

        if (nama == "") {
            $("#lableWrongUser").append('<span class="text-danger"> -Nama User tidak boleh kosong </span><br>')
        }

        if (akses == "") {
            $("#lableWrongUser").append('<span class="text-danger"> -Hak Akses tidak boleh kosong </span><br>')
        }

        if (username != "" && nama != "" && akses != "") {
            $.ajax({
                type : "POST",
                url : '<?=base_url('saveUser')?>',
                data: $("#editUserForm").serialize(),
                success: function(report){
					if (report == 1) {
						swal({
						    type: 'warning',
						    title: 'Data Username sudah ada',
						    showConfirmButton: false,
						    timer: 1500
						});
					}else{
							$('#modalUser').modal('toggle')
							swal({
								type: 'success',
								title: 'Data berhasil di simpan',
								showConfirmButton: false,
								timer: 1500
							});
							$("#userid").val("")
							$("#username").val("")
							$("#namaUser").val("")
							$("#password").val("")
							$("#password").attr("placeholder", "Password")
							$("#status").removeAttr("checked", "checked")
						refreshDataTable(report)
					}
                }
            })
        }
	}

	function getUserDetail(kd){
		$.ajax({
			type : "POST",
			url : '<?=base_url('getUserEdit')?>',
			data : {dataId : kd},
			success: function(res){
				console.log(res)
				var obj = JSON.parse(res);
				console.log(obj)
				$("#userid").val(obj.kode)
				$("#username").val(obj.nama)
				$("#namaUser").val(obj.nama_user)
				$("#password").attr("placeholder", "kosongkan jika tidak diubah")
                if (obj.status == "On") {
    				$("#status").attr("checked", "checked")                
                }else{
    				// $("#status").attr("checked", "checked")                
                }
				$("#jenisUser").val("update")
				$("#titleUser").html("Edit Data Supplier")
			}
		})
	}

	function getUserDelete(kd){
        swal({
            title: 'Delete data?',
            text: "Data akan di hapus",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus',
            cancelButtonText: 'tidak',
            reverseButtons: true
        }).then(function(result){
            if (result.value) {
                $.ajax({
                	type : "POST",
                	url : '<?=base_url('delPengguna')?>',
                	data : {dataId : kd},
                	success: function(res){
                        swal({
                            type: 'success',
                            title: 'Data telah di hapus',
                            showConfirmButton: false,
                            timer: 1500
                        });
						refreshDataTable(res)
                	}
                })
            } else if (result.dismiss === 'cancel') {
                swal({
                    type: 'info',
                    title: 'Data tidak dihapus',
                    showConfirmButton: false,
                    timer: 1500
                });
            }
        });
    }
    
    function getKodeEdit(kode){
		console.log("akses get kode edit")
        getUserDetail(kode)
	}

	function refreshDataTable(res){
		var obj = JSON.parse(res);
		var dataRes = [];
			for (let index = 0; index < obj.length; index++) {
				
				var arraynew = [obj[index].nama_user,obj[index].username,obj[index].nama_akses,obj[index].blokir,'<center><button data-toggle="modal" onclick="getKodeEdit('+obj[index].id_user+')" data-target="#modalUser" kode="'+obj[index].id_user+'" id="btnGetSupp'+obj[index].id_user+'" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"> <i class="flaticon-edit"></i> </button> <button kode="'+obj[index].id_user+'" onclick="getUserDelete('+obj[index].id_user+')" id="btnDelete'+obj[index].id_user+'" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"> <i class="flaticon-delete-1"></i> </button> </center>'];
				dataRes.push(arraynew);   
			}
		// console.log(dataRes)
		$('#User').DataTable( {
			destroy: true,
			data: dataRes,
			columns: [
				{ title: "Nama User" },
				{ title: "Username" },
				{ title: "Hak Akses" },
				{ title: "Status" },
				{ title: "Aksi" }
			]
		} );
	}
</script>
