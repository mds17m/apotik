<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SaveController extends CI_Controller {

	public function saveSup()
	{

        if ($this->input->post('jenis') == 'tambah') {
            $data = array(
                'kode_supplier' => $this->input->post('kode') , 
                'nama_supplier' => $this->input->post('nama') , 
                'alamat' => $this->input->post('alamat') , 
                'telepon' => $this->input->post('telepon') , 
                'created_user' => 2 , 
                'created_date' => date('Y-m-d H:m:s'),  
            );
            $save = $this->Mod_save->saveSupp($data);
        }else{
            $kodeSupp = $this->input->post('kode');
            $data = array(
                'nama_supplier' => $this->input->post('nama') , 
                'alamat' => $this->input->post('alamat') , 
                'telepon' => $this->input->post('telepon') , 
                'updated_user' => 2 , 
                'updated_date' => date('Y-m-d H:m:s'),  
            );
            $save = $this->Mod_save->updateSupp($data,$kodeSupp);

        }
        
        $data = $this->Mod_get->getAllSupplier()->result();
        echo json_encode($data);
        

    }
    
    public function saveObat()
	{
        if ($this->input->post('jenisObat') == 'tambah') {
            $data = array(
                'kode_obat' => $this->input->post('kodeObat') , 
                'nama_obat' => $this->input->post('namaObat') , 
                'harga_beli' => $this->input->post('hargaBeli') , 
                'harga_jual' => $this->input->post('hargaJual') , 
                'satuan' => $this->input->post('satuan') , 
                'min_stok' => $this->input->post('minstok') , 
                'stok' => 0 , 
                'created_user' => 2 , 
                'created_date' => date('Y-m-d H:m:s'),  
            );
            $save = $this->Mod_save->saveObat($data);
        }else{
            $kodeSupp = $this->input->post('kodeObat');
            $data = array( 
                'nama_obat' => $this->input->post('namaObat') , 
                'harga_beli' => $this->input->post('hargaBeli') , 
                'harga_jual' => $this->input->post('hargaJual') , 
                'satuan' => $this->input->post('satuan') , 
                'min_stok' => $this->input->post('minstok') , 
                'stok' => 0 , 
                'updated_user' => 2 , 
                'updated_date' => date('Y-m-d H:m:s'),  
            );
            $save = $this->Mod_save->updateObat($data,$kodeSupp);

        }
        $data = $this->Mod_get->getAllObat()->result();
        echo json_encode($data);
        

    }
    
    public function saveUser()
	{
        $status = "";
        if($this->input->post('status') != ""){
            $status = "On";
        }else{
            $status = "Off";
        }
        
        if ($this->input->post('jenisUser') == 'tambah') {
            $cek = $this->Mod_get->cekUsername($this->input->post('username'))->num_rows();
            if ($cek > 0) {
                echo $cek;
                die();
            }

            $data = array(
                'nama_user' => $this->input->post('namaUser') , 
                'username' => $this->input->post('username') , 
                'password' => md5($this->input->post('password')), 
                'hak_akses' => $this->input->post('hak_akses') , 
                'blokir' => $status, 
                'created_user' => 1 , 
                'created_date' => date('Y-m-d H:m:s'),  
            );
            
            $save = $this->Mod_save->saveUser($data);
            $data = $this->Mod_get->getAllUser()->result();
            echo json_encode($data);
        }else{
            $kodeSupp = $this->input->post('userid');
            if ($this->input->post('password') != "") {
                $data = array( 
                    'nama_user' => $this->input->post('namaUser') , 
                    'username' => $this->input->post('username') , 
                    'password' => md5($this->input->post('password')), 
                    'hak_akses' => $this->input->post('hak_akses') , 
                    'blokir' => $status, 
                    'updated_user' => 2 , 
                    'updated_date' => date('Y-m-d H:m:s'),  
                );
            }else{
                $data = array( 
                    'nama_user' => $this->input->post('namaUser') , 
                    'username' => $this->input->post('username') ,  
                    'hak_akses' => $this->input->post('hak_akses') , 
                    'blokir' => $status, 
                    'updated_user' => 2 , 
                    'updated_date' => date('Y-m-d H:m:s'),  
                );
            }
            $save = $this->Mod_save->updateUser($data,$kodeSupp);
            $data = $this->Mod_get->getAllUser()->result();
            echo json_encode($data);
        }      
    }
    
    public function savePermission(){
        $jenis = $this->input->post('jenisInput');
        if ($jenis == 'tambah') {
            $data = array(
                'nama_akses' => $this->input->post('nama') , 
                'menu' => $this->input->post('menu') , 
                'subMenu' => $this->input->post('SubMenu'),
                'aksi' => '',
                'created_date' => date('Y-m-d H:m:s'),
                'updated_date' => ''
            );
            $save = $this->Mod_save->savePermission($data);
        }else{
            $id = $this->input->post('dataId');
            $data = array(
                'nama_akses' => $this->input->post('nama'), 
                'menu' => (string) $this->input->post('menu'), 
                'subMenu' => (string) $this->input->post('SubMenu'),
                'aksi' => '',
                'created_date' => '',
                'updated_date' => date('Y-m-d H:m:s')
            );
            $save = $this->Mod_save->updatePermission($data,$id);
            echo json_encode($data);
        }

    }

}
