<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if(empty($this->session->userdata('Username'))){
			redirect('/');	
		}

	}

	public function index()
	{
		$this->load->view('home');
    }
    
    public function supplier()
	{
		$data['master'] = 'm-menu__item--open  myActive';
		$this->load->view('supplier',$data);
	}
	public function obat()
	{
		$data['master'] = 'm-menu__item--open myActive';
		$this->load->view('obat',$data);
	}

	public function pengguna()
	{
		$data['master'] = 'm-menu__item--open myActive';
		$data['hakAkses'] = $this->Mod_get->getAllPermission()->result();
		$this->load->view('pengguna',$data);
	}

	public function pembelian()
	{
		$this->load->view('pembelian');
	}
	public function penjualan()
	{
		$this->load->view('penjualan');
	}
	public function laporanStok()
	{
		$this->load->view('laporanStok');
	}
	public function laporanPembelian()
	{
		$this->load->view('laporanPembelian');
	}
	public function laporanPenjualan()
	{
		$this->load->view('laporanPenjualan');
	}
	public function backup()
	{
		$data['listDb'] = $this->Mod_get->getAllDb()->result();
		$this->load->view('backup',$data);
	}

	public function role(){
		$data['master'] = 'm-menu__item--open';
		$data['hakAkses'] = $this->Mod_get->getAllPermission()->result();
		$this->load->view('role',$data);
	}

}
