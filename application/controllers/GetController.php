<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class GetController extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	function index(){
		$this->load->view('user_view');
	}

	function getSupplier()
	{
		$list = $this->Mod_get->getDataSupplier();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $field->kode_supplier;
			$row[] = $field->nama_supplier;
			$row[] = $field->alamat;
			$row[] = $field->telepon;
			$row[] = '<center><button data-toggle="modal" data-target="#m_modal_1" kode="'.$field->kode_supplier.'" id="btnGetSupp'.$field->kode_supplier.'" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"> <i class="flaticon-edit"></i> </button> <button kode="'.$field->kode_supplier.'" id="btnDelete'.$field->kode_supplier.'" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"> <i class="flaticon-delete-1"></i> </button> </center><script type="text/javascript">$(document).ready(function() {$("#btnGetSupp'.$field->kode_supplier.'").click(function(){var kd = $(this).attr("kode");getSupplierDetail(kd);});$("#btnDelete'.$field->kode_supplier.'").click(function(){var kd = $(this).attr("kode");getSupplierDelete(kd);});});</script>';


			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Mod_get->countAllSupp(),
			"recordsFiltered" => $this->Mod_get->countSupplier(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function getObat()
	{
		$list = $this->Mod_get->getDataObat();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $field->kode_obat;
			$row[] = $field->nama_obat;			
			$row[] = $field->harga_beli;
			$row[] = $field->harga_jual;
			$row[] = $field->nama_satuan;
			$row[] = $field->min_stok;
			$row[] = $field->stok;
			$row[] = '<center><button data-toggle="modal" data-target="#modal_obat" kode="'.$field->kode_obat.'" id="btnGetSupp'.$field->kode_obat.'" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"> <i class="flaticon-edit"></i> </button> <button kode="'.$field->kode_obat.'" id="btnDelete'.$field->kode_obat.'" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"> <i class="flaticon-delete-1"></i> </button> </center><script type="text/javascript">$(document).ready(function() {$("#btnGetSupp'.$field->kode_obat.'").click(function(){var kd = $(this).attr("kode");getObatDetail(kd);});$("#btnDelete'.$field->kode_obat.'").click(function(){var kd = $(this).attr("kode");getObatDelete(kd);});});</script>';


			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Mod_get->countAllObat(),
			"recordsFiltered" => $this->Mod_get->countObat(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function getMinimum()
	{
		$list = $this->Mod_get->getDataMinimum();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $field->kode_obat;
			$row[] = $field->nama_obat;			
			$row[] = $field->harga_beli;
			$row[] = $field->harga_jual;
			$row[] = $field->nama_satuan;
			$row[] = $field->min_stok;
			$row[] = $field->stok;
			$row[] = '<center><button data-toggle="modal" data-target="#modal_obat" kode="'.$field->kode_obat.'" id="btnGetSupp'.$field->kode_obat.'" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"> <i class="flaticon-edit"></i> </button> <button kode="'.$field->kode_obat.'" id="btnDelete'.$field->kode_obat.'" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"> <i class="flaticon-delete-1"></i> </button> </center><script type="text/javascript">$(document).ready(function() {$("#btnGetSupp'.$field->kode_obat.'").click(function(){var kd = $(this).attr("kode");getObatDetail(kd);});$("#btnDelete'.$field->kode_obat.'").click(function(){var kd = $(this).attr("kode");getObatDelete(kd);});});</script>';


			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Mod_get->countAllObat(),
			"recordsFiltered" => $this->Mod_get->countObat(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function getUser()
	{
		$list = $this->Mod_get->getDataUser();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $field) {
			$no++;
			$row = array();
			$row[] = $field->nama_user;
			$row[] = $field->username;			
			$row[] = $field->nama_akses;
			$row[] = $field->blokir;
			$row[] = '<center><button data-toggle="modal" data-target="#modalUser" kode="'.$field->id_user.'" id="btnGetSupp'.$field->id_user.'" class="btn btn-success m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"> <i class="flaticon-edit"></i> </button> <button kode="'.$field->id_user.'" id="btnDelete'.$field->id_user.'" class="btn btn-danger m-btn m-btn--icon btn-sm m-btn--icon-only  m-btn--pill m-btn--air"> <i class="flaticon-delete-1"></i> </button> </center><script type="text/javascript">$(document).ready(function() {$("#btnGetSupp'.$field->id_user.'").click(function(){var kd = $(this).attr("kode");getUserDetail(kd);});$("#btnDelete'.$field->id_user.'").click(function(){var kd = $(this).attr("kode");getUserDelete(kd);});});</script>';


			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Mod_get->countAllUser(),
			"recordsFiltered" => $this->Mod_get->countUser(),
			"data" => $data,
		);
		//output dalam format JSON
		echo json_encode($output);
	}

	public function getKode(){
		$data = $this->Mod_get->getKodeSup();
		if($data->num_rows() > 0){
			foreach ($data->result() as $valData) {}
			$kd = $valData->kode_supplier;
			$Nokd = str_replace('SP','',$kd);
			$newKd = (int)$Nokd + 1;
			
		}else{
			$newKd = '1';
		}
		echo $newKd;
	}

	public function getKodeObat(){
		$data = $this->Mod_get->getKodeObat();
		if($data->num_rows() > 0){
			foreach ($data->result() as $valData) {}
			$kd = $valData->kode_obat;
			$Nokd = str_replace('OB','',$kd);
			$newKd = (int)$Nokd + 1;
			echo $newKd;
		}else{
			echo '1';
		}
	}

	public function getSupplierEdit(){
		$kode = $this->input->post('dataId');
		$data = $this->Mod_get->getSupplierEdit($kode)->result();
		foreach ($data as $dataValue) {}

		echo json_encode(
				array(
					'kode' => $dataValue->kode_supplier, 
					'nama' => $dataValue->nama_supplier, 
					'telp' => $dataValue->telepon, 
					'alamat' => $dataValue->alamat, 
				)
			);
	}
	

	public function getObatEdit(){
		$kode = $this->input->post('dataId');
		$data = $this->Mod_get->getObatEdit($kode)->result();
		foreach ($data as $dataValue) {}

		echo json_encode(
				array(
					'kode' => $dataValue->kode_obat, 
					'nama' => $dataValue->nama_obat, 
					'minstok' => $dataValue->min_stok, 
					'hargabeli' => $dataValue->harga_beli, 
					'hargajual' => $dataValue->harga_jual, 
					'satuan' => $dataValue->satuan, 
				)
			);
	}


	public function getUserEdit(){
		$kode = $this->input->post('dataId');
		$data = $this->Mod_get->getUserEdit($kode)->result();
		foreach ($data as $dataValue) {}

		echo json_encode(
				array(
					'kode' => $dataValue->id_user, 
					'nama' => $dataValue->username, 
					'password' => $dataValue->password, 
					'nama_user' => $dataValue->nama_user, 
					'hak_akses' => $dataValue->hak_akses, 
					'status' => $dataValue->blokir, 
				)
			);
	}

	public function getDelSup(){
		$kodeSupp = $this->input->post('dataId');
		$delete = $this->Mod_delete->delSupp($kodeSupp);
		$data = $this->Mod_get->getAllSupplier()->result();
        echo json_encode($data);
	}

	public function getDelUser(){
		$kodeSupp = $this->input->post('dataId');
		$delete = $this->Mod_delete->delUser($kodeSupp);
		$data = $this->Mod_get->getAllUser()->result();
        echo json_encode($data);
	}

	public function getDelObat(){
		$kodeSupp = $this->input->post('dataId');
		$delete = $this->Mod_delete->delObat($kodeSupp);
		$data = $this->Mod_get->getAllObat()->result();
        echo json_encode($data);
	}

	public function prosesBackup()
	{
		$this->load->dbutil();
		$prefs = array(
			'format' => 'zip',
			'filename' => 'apotek.sql'
		);
		$backup = $this->dbutil->backup($prefs);
		$db_name = date("Ymd_His") . '_database'.'.sql.gz';
		$save = 'backup/' . $db_name;
		$this->load->helper('file');
		$test = write_file($save, $backup);
		if ($test) {
			$data = array(
				'nama_file' => $db_name,
				'ukuran_file' => '',
				'created_user' => $this->session->userdata('userid'),
				'created_date' => date('Y-m-d H:m:s')
			);

			$saveDB = $this->Mod_save->savedb($data);
			redirect('/database');
		}
	}

	public function getDb(){
		$id = $this->uri->segment('3');
		$dataDb = $this->Mod_get->getDbWhere($id)->result();
		foreach ($dataDb as $valDb) {}
		$namafile = $valDb->nama_file;
		$this->load->helper('download');
		force_download($namafile,$namafile);

	}

	public function getDelDb(){
		$id = $this->input->post('dataId');
		$datadb = $this->Mod_get->getDbWhere($id)->result();
		$delete = $this->Mod_delete->delDb($id);
		$data =  $this->Mod_get->getAllDb()->result();
		echo json_encode($data);
	}

	public function getDelPer(){
		$id = $this->input->post('dataId');
		$delete = $this->Mod_delete->delPer($id);
	}

	public function getPermission(){
		$id = $this->input->post('dataId');
		$data = $this->Mod_get->getWherPer($id)->result();
		echo json_encode($data);
	}

	

}
